
var gulp         = require('gulp'),
    path         = require('path'),
    plumber      = require('gulp-plumber'),
    header       = require('gulp-header'),
    browsersync  = require('browser-sync'),
    filter       = require('gulp-filter'),
    sass         = require('gulp-ruby-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglify'),
    minify       = require('gulp-minify-html'),
    notify       = require('gulp-notify'),
    markdown     = require('gulp-markdown-docs'),
    changed      = require('gulp-changed'),
    pkg          = require('./package.json');

var devInfo =
[
    '',
    '/*!',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @author <%= pkg.author %>',
    ' * @contributors <%= pkg.contributors %>',
    ' * @version v<%= pkg.version %>',
    ' * @link <%= pkg.url %>',
    ' * @license <%= pkg.license %>',
    ' */',
    '',
    ''
].join('\n');

var docs = [ 'README.md' ],
    html = [ 'component.html', 'preview/*/index.html' ],
    scss = [ 'assets/styles/component.scss' ],
    js   = [ 'assets/scripts/*.js' ];

gulp.task('sync', function ()
{
    browsersync(
    {
        server: {
            baseDir: '../'
        },
        open: false,
        notify: false,
        startPath: '/multilevel-header/',
        browser: 'google chrome'
    });
});

gulp.task('styles', function ()
{
    var filtered = filter(['*.css', '!*.map']);

    return sass(scss,
    {
        style: 'compressed',
        precision: 4
    })
    .pipe(plumber())
    .pipe(filtered) // Ignore source maps for autoprefixer
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(autoprefixer(
    {
        browsers: ['last 3 versions'],
        cascade: false
    }))
    .pipe(sourcemaps.write('./', { includeContent: false, sourceRoot: './src' }))
    .pipe(filtered.restore()) // Restore all files to move them
    .pipe(header(devInfo, { pkg: pkg }))
    .pipe(gulp.dest('assets/styles/dist')) // Write the CSS & source maps
    // .pipe(filtered) // Filtering (again) to stream only CSS files
    .pipe(browsersync.reload({ stream: true }))
    .pipe(notify('Your CSS file was modified.'));
});

gulp.task('scripts', function()
{
    return gulp.src(js)
    .pipe(changed('assets/scripts/dist'))
    .pipe(plumber())
    .pipe(concat('component.js'))
    .pipe(uglify())
    .pipe(header(devInfo, { pkg: pkg }))
    .pipe(gulp.dest('assets/scripts/dist'))
    .pipe(notify('Your JS file was modified.'));
});

gulp.task('reload', function ()
{
    browsersync.reload();
});

gulp.task('docs', function ()
{
    return gulp.src(docs)
    .pipe(markdown('index.html', {
        highlightTheme: 'tomorrow-night',
        categorySort: 'alphabetical',
        templatePath: path.resolve('../') + '/libs/assets/templates/docs.html'
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('build', function ()
{
    var inlinerOpt = {
        compress: true,
        swallowErrors: false,
        rootpath: path.resolve('../')
    };

    var minifyOpt = {
        empty: true,
        comments: false,
        quotes: true
    };

    return gulp.src('component.html')
    .pipe(minify(minifyOpt))
    .pipe(concat('build.html'))
    .pipe(gulp.dest('./'))
    .pipe(notify('Your component was built.'));
});

gulp.task('default', ['sync'], function ()
{
    gulp.watch(docs, ['docs', 'reload']);
    gulp.watch(html, ['build', 'reload']);
    gulp.watch(scss, ['styles']);
    gulp.watch(js,   ['scripts', 'reload']);
});
