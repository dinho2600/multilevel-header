(function() {
 
    'use strict';

    window.app = window.app || angular.module('app', []);

    var templateUI = '<div class="mh-container" horizontal layout flex ng-show="active" ng-class="{\'ready\' : ready}"><div class="mh-center"><div class="mh-user-seller" flex ng-show="seller"><div class="mh-menu" flex><div medium-large-devices><ul horizontal layout><li><a href="http://blog.polishop.com.vc" target="_blank">Blog</a></li><li><a href="/eventos">Eventos</a></li><li><a href="http://apps.polishop.com.br/empreendedor" target="_blank">My Office</a></li><li><a href="http://apps.polishop.com.br/empreendedor/MyOffice" target="_blank">Binário</a></li><li><a href="/atendimento/fale-conosco">Fale Conosco</a></li></ul></div><form small-devices><select ng-model="link" ng-change="goToLink(link);"><option value>Selecione</option><option value="http://blog.polishop.com.vc">Blog</option><option value="/eventos">Eventos</option><option value="http://apps.polishop.com.br/empreendedor">My Office</option><option value="http://apps.polishop.com.br/empreendedor/MyOffice">Binário</option><option value="/atendimento/fale-conosco">Fale Conosco</option></select></form></div></div><div class="mh-user-not-logged" horizontal layout ng-hide="seller"><div class="mh-public-message" flex><p>Quer uma loja dessas só pra você?<br>CLIQUE <a href="/oportunidade">AQUI</a></p></div><div class="mh-menu" flex two><div medium-large-devices><ul horizontal layout><li><a href="/oportunidade">Oportunidade</a></li><li><a href="/empresa">A Empresa</a></li><li><a href="/eventos">Eventos</a></li><li><a href="http://www.polishop.com.vc/site/Cadastro.aspx">Cadastre-se</a></li></ul></div><form small-devices><select ng-model="link" ng-change="goToLink(link);"><option value>Selecione</option><option value="/oportunidade">Oportunidade</option><option value="/empresa">A Empresa</option><option value="/eventos">Eventos</option><option value="http://www.polishop.com.vc/site/Cadastro.aspx">Cadastre-se</option></select></form></div></div></div></div>';

        app.directive('multilevelHeader', ['$timeout', '$http', function ($timeout, $http) {

            return {

                restrict: 'E',
                scope: {},
                transclude: true,
                template: templateUI,
                controllerAs: 'multilevelHeader',

                controller: ['$scope', function (scope) {

                    scope.appKey = 'suporte.internet@polishop.com.br';
                    scope.token  = 'P0l!' + (5 * 5 - 5) + (5 * 3) + 'web';

                    scope.appHeader = {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept': 'text/html,application/json,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'X-VTEX-API-AppKey': scope.appKey,
                        'X-VTEX-API-AppToken': scope.token
                    };

                    scope.active = (location.host === "www.polishop.vc");
                    scope.seller = false;
                    scope.ready = false;

                    var query = location.pathname.replace('/store/','');

                    $http({
                        method: 'GET',
                        url: '/api/addon/pvt/giftlist/getbyurlfolder/' + query,
                        headers: scope.appHeader
                    }).
                    success(function (response, status, headers, config) {
 
                        if(response.giftListTypeName === 'Empreendedor' || response.giftListTypeName === 'Empreendedor (NEW)') {
                            
                            $http({
                                method: 'GET',
                                url: '/no-cache/profileSystem/getProfile'
                            }).
                            success(function (response) {

                                if(response.IsUserDefined === true) {
                                   
                                   scope.seller = true;

                                }

                                scope.ready = true;

                            });
                        
                        }

                    });

                    scope.goToLink = function(link) {

                        if(link.length > 0) {

                            location.href = link;

                        }

                    };
            
                }],
                link: function (scope, elements, attributes, controller) {

                    var i,
                        $host = elements[0],
                        $content = $host.querySelector('[include]');

                    for (var key in attributes) {
                        scope[key] = attributes[key];
                    }

                    $timeout(function () {


                    },0);
                }
            };

        }]);

})();